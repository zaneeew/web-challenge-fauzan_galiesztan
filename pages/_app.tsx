import "@/styles/main.css";
import type { AppProps } from "next/app";
import Head from "next/head";
import { useRouter } from "next/router";
export default function App({ Component, pageProps }: AppProps) {
  const router = useRouter();
  return (
    <>
      <Head>
        <title>
          {router.asPath.substring(1).charAt(0).toUpperCase() +
            router.asPath.slice(2)}
        </title>
      </Head>
      <Component {...pageProps} />
    </>
  );
}
