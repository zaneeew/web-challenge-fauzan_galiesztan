import React, { useState, ChangeEvent, FormEvent } from "react";
import { GetServerSideProps } from "next";
import Cookies from "js-cookie";
import apiServices from "@/services/api";
import { useRouter } from "next/router";
import Image from "next/image";
import { Toaster, toast } from "react-hot-toast";
import logoImg from "@/styles/assets/logo.png";
import headerLogo from "@/styles/assets/header-login.png";

const login = () => {
  const router = useRouter();
  const [formData, setFormData] = useState({ username: "", password: "" });
  const [loading, setLoading] = useState(false);

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleLogin = async (e: FormEvent) => {
    e.preventDefault();
    setLoading(true);
    try {
      const response = await apiServices.postData("/auth/login", formData);

      if (response.token) {
        const expirationInMilliseconds = response.expires_in * 1000;
        const expirationDate = new Date();

        expirationDate.setTime(
          expirationDate.getTime() + expirationInMilliseconds
        );

        Cookies.set("token", response.token, {
          expires: expirationDate,
        });

        toast.success("Login Success!");
        setTimeout(() => {
          router.push("/dashboard");
        }, 1500);
      } else {
        setLoading(false);
        toast.error("Login Failed! No token in response!");
      }
    } catch (error) {
      setLoading(false);
      toast.error("Username or Password wrong!");
    }
  };
  return (
    <section className="border-red-500 bg-gray-200 min-h-screen flex items-center justify-center">
      <Toaster />
      <div className="bg-white p-5 flex rounded-2xl shadow-lg w-96 md:max-w-3xl md:w-auto relative">
        <Image
          src={headerLogo}
          width={90}
          height={35}
          alt="logo"
          priority
          className="absolute left-0 top-0 rounded-tl-2xl"
        />
        <div className="md:w-1/2 w-full flex flex-col justify-center items-center md:pr-5">
          <Image src={logoImg} width={90} height={35} alt="logo" priority />
          <div className="mt-12 w-full">
            <p className="text-lg font-bold">Login</p>
            <p className="text-xs pt-2">Please sign in to continue.</p>
          </div>
          <form className="mt-6 w-full" onSubmit={handleLogin}>
            <div>
              <label className="block text-gray-700 text-xs">User ID</label>
              <input
                type="text"
                name="username"
                onChange={handleChange}
                placeholder="User ID"
                className="w-full pr-4 py-3 mt-2 text-xs placeholder:italic border border-t-0 border-x-0 focus:border-blue-500 bg-white focus:outline-none"
                required
              />
            </div>
            <div className="mt-4">
              <label className="block text-gray-700 text-xs">Password</label>
              <input
                type="password"
                name="password"
                onChange={handleChange}
                placeholder="Password"
                minLength={3}
                className="w-full pr-4 py-3 mt-2 text-xs placeholder:italic border border-t-0 border-x-0 focus:border-blue-500 bg-white focus:outline-none"
                required
              />
            </div>
            <button
              disabled={loading}
              type="submit"
              className="w-28 float-right block bg-[#6338a1] disabled:cursor-not-allowed text-white font-semibold rounded-3xl
          p-2 mt-6"
            >
              LOGIN
            </button>
          </form>
          <div className="mt-24">
            <p className="text-xs text-gray-500">
              Don't have an account?{" "}
              <span className=" text-[#ed4510]">Sign Up</span>
            </p>
          </div>
        </div>
        <div className="w-1/2 md:block hidden ">
          <img
            src="https://img.freepik.com/free-vector/uploading-concept-illustration_114360-2658.jpg?w=2000"
            className="rounded-2xl"
            alt="page img"
          />
        </div>
      </div>
    </section>
  );
};

export const getServerSideProps: GetServerSideProps = async (context) => {
  const cookies = context.req.headers.cookie || "";
  const tokenCookie = cookies
    .split(";")
    .find((cookie: string) => cookie.trim().startsWith("token="));

  if (tokenCookie) {
    return {
      redirect: {
        destination: "/dashboard",
        permanent: false,
      },
    };
  }

  return {
    props: {},
  };
};

export default login;
