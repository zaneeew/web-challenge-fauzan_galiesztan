import React, { useState, useEffect } from "react";
import { GetServerSideProps } from "next";
import LayoutAdmin from "@/layouts/LayoutAdmin";
import apiServices from "@/services/api";
import { AddBoxOutlined } from "@mui/icons-material";
import Product from "@/components/Tables/Product";
import Add from "@/components/Modal/Add";

const Dashboard = () => {
  const [tableProduct, setTableProduct] = useState([]);
  const [openAdd, setOpenAdd] = useState(false);

  const fetchTable = async () => {
    try {
      const responseTable = await apiServices.getData("/products");
      setTableProduct(responseTable.products);
    } catch (err) {
      console.error("Error fetching data Table: " + err);
    }
  };

  useEffect(() => {
    fetchTable();
  }, []);

  return (
    <>
      <LayoutAdmin>
        <div className="w-full flex flex-row justify-between items-center">
          <h1 className="font-bold">Data User</h1>
          <div>
            <button
              className="p-1 bg-blue-500 text-white rounded-md !w-32 flex justify-center items-center gap-1"
              onClick={() => setOpenAdd(true)}
            >
              <AddBoxOutlined className="text-lg" />
              Add Product
            </button>
          </div>
        </div>
        <Add open={openAdd} handleClose={() => setOpenAdd(false)} />
        <Product data={tableProduct} />
      </LayoutAdmin>
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async (context) => {
  const cookies = context.req.headers.cookie || "";
  const tokenCookie = cookies
    .split(";")
    .find((cookie: string) => cookie.trim().startsWith("token="));

  if (!tokenCookie) {
    return {
      redirect: {
        destination: "/login",
        permanent: false,
      },
    };
  }
  return {
    props: {},
  };
};

export default Dashboard;
