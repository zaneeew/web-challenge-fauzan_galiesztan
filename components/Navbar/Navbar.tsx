import React, { useState } from "react";
import { useRouter } from "next/router";
import Image from "next/image";
import Link from "next/link";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import Cookies from "js-cookie";
import toast from "react-hot-toast";

const Navbar = () => {
  const router = useRouter();
  const [open, setOpen] = useState(false);

  const handleLogout = async () => {
    try {
      await Cookies.remove("token");
      toast.success("Success Logout");
      setTimeout(() => {
        router.push("/login");
      }, 500);
    } catch {
      console.error("error delete token");
    }
  };

  return (
    <header className="flex items-center justify-center shadow-md">
      <div className="max-w-7xl w-full p-6 flex flex-row justify-between items-center">
        <div className="flex flex-row justify-start items-center gap-8">
          <Link href={"/dashboard"}>
            <Image
              src={"/db.jpg"}
              width={140}
              height={35}
              alt="logo"
              priority
            />
          </Link>
        </div>
        <div className="relative">
          <AccountCircleIcon
            className={`text-${
              open ? "blue-600" : "black"
            } text-3xl cursor-pointer`}
            onClick={() => setOpen(!open)}
          />
          <div
            className={`${
              open ? "block" : "hidden"
            } absolute right-0 bg-white p-3 shadow-md rounded-md border border-solid border-[#dedede]`}
          >
            <ul className="flex flex-col justify-start items-start gap-2 cursor-pointer">
              <li className="text-sm" onClick={handleLogout}>
                Logout
              </li>
            </ul>
          </div>
        </div>
      </div>
    </header>
  );
};

export default Navbar;
