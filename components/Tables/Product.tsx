import { useState } from "react";
import {
  KeyboardArrowLeft,
  KeyboardArrowRight,
  CreateOutlined,
  VisibilityOutlined,
  DeleteOutline,
} from "@mui/icons-material";
import Detail from "../Modal/Detail";
import Edit from "../Modal/Edit";
import Delete from "../Modal/Delete";
import { Products, PaginationTableProps } from "@/interfaces";

const ITEMS_PER_PAGE = 5;

const Product = ({ data }: PaginationTableProps) => {
  const [openDetail, setOpenDetail] = useState(false);
  const [openEdit, setOpenEdit] = useState(false);
  const [openDelete, setOpenDelete] = useState(false);
  const [selectItem, setSelectedItem] = useState({} as Products);
  const [currentPage, setCurrentPage] = useState(1);

  const totalPages = Math.ceil(data.length / ITEMS_PER_PAGE);

  const paginatedData = data.slice(
    (currentPage - 1) * ITEMS_PER_PAGE,
    currentPage * ITEMS_PER_PAGE
  );

  const handlePageChange = (page: number) => {
    setCurrentPage(page);
  };

  const tableRows = paginatedData.map((item, index) => (
    <tr key={index}>
      <td className="text-center">{item.title}</td>
      <td className="text-center">${item.price}</td>
      <td className="text-center">{item.discountPercentage}%</td>
      <td className="text-center">{item.rating}</td>
      <td className="text-center">{item.stock}</td>
      <td className="text-center">{item.brand}</td>
      <td className="text-center">{item.category}</td>
      <td className="flex justify-between items-center">
        <button
          className="p-1 bg-green-600 rounded-md text-white"
          onClick={() => {
            setSelectedItem(item);
            setOpenEdit(true);
          }}
        >
          <CreateOutlined className="text-lg" />
        </button>
        <button
          className="p-1 bg-blue-600 rounded-md text-white"
          onClick={() => {
            setSelectedItem(item);
            setOpenDetail(true);
          }}
        >
          <VisibilityOutlined className="text-lg" />
        </button>
        <button
          className="p-1 bg-red-600 rounded-md text-white"
          onClick={() => {
            setSelectedItem(item);
            setOpenDelete(true);
          }}
        >
          <DeleteOutline className="text-lg" />
        </button>
      </td>
    </tr>
  ));

  return (
    <>
      <Detail
        open={openDetail}
        handleClose={() => setOpenDetail(false)}
        item={selectItem}
      />
      <Edit
        open={openEdit}
        handleClose={() => setOpenEdit(false)}
        item={selectItem}
      />
      <Delete
        open={openDelete}
        handleClose={() => setOpenDelete(false)}
        item={selectItem}
      />
      <div className="w-full flex flex-col gap-4 justify-start items-start">
        <div className="w-full border border-solid border-[#dedede] rounded-xl">
          <table className="!text-xs !text-center">
            <thead>
              <tr>
                <th className="text-center">Title</th>
                <th className="text-center">Price</th>
                <th className="text-center">Discount</th>
                <th className="text-center">Rating</th>
                <th className="text-center">Stock</th>
                <th className="text-center">Brand</th>
                <th className="text-center">Category</th>
                <th className="text-center">Actions</th>
              </tr>
            </thead>
            <tbody>{tableRows}</tbody>
          </table>
        </div>
        <div className="w-full flex flex-row justify-end items-center gap-6">
          <div className="flex flex-row gap-2 justify-between">
            <button
              onClick={() => handlePageChange(currentPage - 1)}
              disabled={currentPage === 1}
              className="p-2 border border-solid border-[#dedede] rounded-md"
            >
              <KeyboardArrowLeft />
            </button>
            <div className="py-2 px-4 font-semibold border border-solid border-[#dedede] rounded-md">
              {currentPage}
            </div>
            <button
              onClick={() => handlePageChange(currentPage + 1)}
              disabled={currentPage === totalPages}
              className="p-2 border border-solid border-[#dedede] rounded-md"
            >
              <KeyboardArrowRight />
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

export default Product;
