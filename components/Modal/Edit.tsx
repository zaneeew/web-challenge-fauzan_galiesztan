import React, { useState, useEffect, FormEvent } from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import apiServices from "@/services/api";
import toast from "react-hot-toast";
import { DetailProps } from "@/interfaces";

const Edit = ({ open, handleClose, item }: DetailProps) => {
  const initialFormData = {
    title: item.title || "",
    price: String(item.price || 0),
    discountPercentage: String(item.discountPercentage || 0),
    rating: String(item.rating || 0),
    stock: String(item.stock || 0),
    brand: item.brand || "",
    category: item.category || "",
  };

  const [formData, setFormData] = useState(initialFormData);

  useEffect(() => {
    setFormData(initialFormData);
  }, [item]);

  const handleInputChange = (
    e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>
  ) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleSubmit = async (e: FormEvent) => {
    e.preventDefault();
    try {
      const params = {
        title: formData.title,
        price: parseFloat(formData.price),
        discountPercentage: parseFloat(formData.discountPercentage),
        rating: parseFloat(formData.rating),
        stock: parseInt(formData.stock),
        brand: formData.brand,
        category: formData.category,
      };
      const response = await apiServices.putData(
        `/products/${item.id}`,
        params
      );

      if (response) {
        toast.success("Success Update Data");
        handleClose();
      } else {
        toast.error("Failed Update Data");
      }
    } catch (error) {
      toast.error("Failed Update Data" + error);
    }
  };

  return (
    <>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle className="text-center text-base">
          Edit Product
        </DialogTitle>
        <form onSubmit={handleSubmit}>
          <DialogContent>
            <div className="grid grid-cols-2 gap-2 gap-x-4 text-xs">
              <div>
                <label className="block text-gray-700">Title*</label>
                <input
                  type="text"
                  name="title"
                  onChange={handleInputChange}
                  required
                  value={formData?.title}
                  className="w-full p-2 rounded-lg bg-gray-200 mt-2 border focus:border-blue-500"
                />
              </div>
              <div>
                <label className="block text-gray-700">Price*</label>
                <input
                  type="number"
                  name="price"
                  onChange={handleInputChange}
                  required
                  value={formData?.price}
                  className="w-full p-2 rounded-lg bg-gray-200 mt-2 border focus:border-blue-500"
                />
              </div>
              <div>
                <label className="block text-gray-700">
                  Discount Percentage*
                </label>
                <input
                  type="number"
                  name="discountPercentage"
                  onChange={handleInputChange}
                  required
                  value={formData?.discountPercentage}
                  className="w-full p-2 rounded-lg bg-gray-200 mt-2 border focus:border-blue-500"
                />
              </div>
              <div>
                <label className="block text-gray-700">Rating*</label>
                <input
                  type="number"
                  name="rating"
                  onChange={handleInputChange}
                  required
                  value={formData?.rating}
                  className="w-full p-2 rounded-lg bg-gray-200 mt-2 border focus:border-blue-500"
                />
              </div>
              <div>
                <label className="block text-gray-700">Stock*</label>
                <input
                  type="number"
                  name="stock"
                  onChange={handleInputChange}
                  required
                  value={formData?.stock}
                  className="w-full p-2 rounded-lg bg-gray-200 mt-2 border focus:border-blue-500"
                />
              </div>
              <div>
                <label className="block text-gray-700">Brand*</label>
                <input
                  type="text"
                  name="brand"
                  onChange={handleInputChange}
                  required
                  value={formData?.brand}
                  className="w-full p-2 rounded-lg bg-gray-200 mt-2 border focus:border-blue-500"
                />
              </div>
              <div>
                <label className="block text-gray-700">Category*</label>
                <input
                  type="text"
                  name="category"
                  onChange={handleInputChange}
                  required
                  value={formData?.category}
                  className="w-full p-2 rounded-lg bg-gray-200 mt-2 border focus:border-blue-500"
                />
              </div>
            </div>
          </DialogContent>
          <DialogActions>
            <Button
              variant="outlined"
              color="error"
              onClick={handleClose}
              className="!text-xs"
            >
              BATAL
            </Button>
            <button
              type="submit"
              className="!text-xs bg-green-600 hover:bg-green-300 text-white py-1.5 px-5 rounded-md font-semibold"
            >
              SIMPAN
            </button>
          </DialogActions>
        </form>
      </Dialog>
    </>
  );
};

export default Edit;
