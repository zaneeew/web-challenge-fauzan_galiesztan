import React from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import { DetailProps } from "@/interfaces";

const Detail = ({ open, handleClose, item }: DetailProps) => {
  return (
    <Dialog open={open} onClose={handleClose}>
      <DialogTitle className="text-center text-base">
        Detail Product
      </DialogTitle>
      <DialogContent>
        <div className="grid grid-cols-2 gap-2 gap-x-4 text-xs">
          <div>
            <label className="block text-gray-700">Title*</label>
            <input
              type="text"
              name="title"
              disabled
              value={item.title}
              className="w-full p-2 rounded-lg bg-gray-200 mt-2 border focus:border-blue-500"
            />
          </div>
          <div>
            <label className="block text-gray-700">Price*</label>
            <input
              type="number"
              name="price"
              disabled
              value={item.price}
              className="w-full p-2 rounded-lg bg-gray-200 mt-2 border focus:border-blue-500"
            />
          </div>
          <div>
            <label className="block text-gray-700">Discount Percentage*</label>
            <input
              type="number"
              name="discountPercentage"
              disabled
              value={item.discountPercentage}
              className="w-full p-2 rounded-lg bg-gray-200 mt-2 border focus:border-blue-500"
            />
          </div>
          <div>
            <label className="block text-gray-700">Rating*</label>
            <input
              type="number"
              name="rating"
              disabled
              value={item.rating}
              className="w-full p-2 rounded-lg bg-gray-200 mt-2 border focus:border-blue-500"
            />
          </div>
          <div>
            <label className="block text-gray-700">Stock*</label>
            <input
              type="number"
              name="stock"
              disabled
              value={item.stock}
              className="w-full p-2 rounded-lg bg-gray-200 mt-2 border focus:border-blue-500"
            />
          </div>
          <div>
            <label className="block text-gray-700">Brand*</label>
            <input
              type="text"
              name="brand"
              disabled
              value={item.brand}
              className="w-full p-2 rounded-lg bg-gray-200 mt-2 border focus:border-blue-500"
            />
          </div>
          <div>
            <label className="block text-gray-700">Category*</label>
            <input
              type="text"
              name="category"
              disabled
              value={item.category}
              className="w-full p-2 rounded-lg bg-gray-200 mt-2 border focus:border-blue-500"
            />
          </div>
        </div>
      </DialogContent>
      <DialogActions>
        <Button
          variant="outlined"
          color="error"
          onClick={handleClose}
          className="!text-xs"
        >
          TUTUP
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default Detail;
