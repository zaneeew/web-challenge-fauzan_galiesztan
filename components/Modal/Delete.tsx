import React from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogContentText from "@mui/material/DialogContentText";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import apiServices from "@/services/api";
import toast, { Toaster } from "react-hot-toast";
import { DetailProps } from "@/interfaces";

const Delete = ({ open, handleClose, item }: DetailProps) => {
  const handleSubmit = async () => {
    try {
      const response = await apiServices.deleteData(`/products/${item.id}`);

      if (response) {
        toast.success("Success Delete Data");
        handleClose();
      } else {
        toast.error("Failed Delete Data");
      }
    } catch (error) {
      toast.error("Failed Delete Data" + error);
    }
  };

  return (
    <>
      <Toaster />
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle className="text-center text-base">
          Confrimation Delete Product
        </DialogTitle>
        <DialogContent>
          <DialogContentText
            id="alert-dialog-slide-description"
            className="!text-sm"
          >
            Apakah anda yakin menghapus data {item.title}?
          </DialogContentText>
        </DialogContent>
        <DialogActions className="flex justify-center items-center">
          <button
            onClick={handleClose}
            className="!text-xs bg-red-600 hover:bg-red-300 text-white py-1.5 px-5 rounded-md font-semibold"
          >
            TIDAK
          </button>
          <Button
            variant="outlined"
            color="error"
            onClick={handleSubmit}
            className="!text-xs"
          >
            IYA
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default Delete;
