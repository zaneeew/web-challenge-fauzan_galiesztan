import React from "react";
import Navbar from "@/components/Navbar/Navbar";

interface LayoutProps {
  children: React.ReactNode;
}

const LayoutAdmin = ({ children }: LayoutProps) => {
  return (
    <>
      <Navbar />
      <section>
        <div className="w-full max-w-[1024px] mx-auto p-6 flex flex-col justify-start items-start gap-4">
          {children}
        </div>
      </section>
    </>
  );
};

export default LayoutAdmin;
