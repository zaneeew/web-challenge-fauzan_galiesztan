export interface Products {
  id: number;
  title: string;
  price: number;
  discountPercentage: number;
  rating: number;
  stock: number;
  brand: string;
  category: string;
}

export interface PaginationTableProps {
  data: Products[];
}

export interface DetailProps {
  handleClose: () => void;
  open: boolean;
  item: Products;
}

export interface AddProps {
  handleClose: () => void;
  open: boolean;
}
