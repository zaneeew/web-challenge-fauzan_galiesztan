# Dashboard

## Usage

```bash
# install dependencies
npm install

# run the Next.js application for Development
npm run dev

# run the Next.js application for Production
npm run build

npm run start
```

## Environment Variables

Here is a description of the environment variables used in the project frontend:

- `NEXT_PUBLIC_BASE_API`: The URL of the local API for the application.
